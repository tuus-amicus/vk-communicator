import time
import interface.captions as capt

class Albums:
    items = None
    currentOffset = 0

    def __init__(self, album):
        self.items = album

    def iterate_next(self, limit):
        tmp = self.items[self.currentOffset: self.currentOffset + limit]
        self.currentOffset += limit
        return tmp

    def count(self):
        return len(self.items)

    def show_titles_with_id(self):
        print(capt.Albums.found_number.format(self.count()))
        time.sleep(1)
        print(capt.Albums.id_title)
        time.sleep(1)

        limit = 10 % self.count() if 10 % self.count() != 0 else self.count()

        while True:
            iterated = self.iterate_next(limit)
            if len(iterated) == 0:
                print(capt.Albums.empty)
                break

            for album in iterated:
                print("| {} -- {} |\n".format(
                    album['id'],
                    album['title']
                ))

            read = input( (capt.Albums.show_next + capt.General.apply_or_decline).format(
                    limit))

            if 'n' in read:
                break