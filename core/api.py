import vk_api
import exceptions


class VkApi:
    api = None
    token = None

    def new_session_with_token(self, token):
        self.__init__(token)

    def __init__(self, token):
        self.token = token
        self.api = vk_api.VkApi(token=token).get_api()

    def get_photos_count_in_group_album(self, group_name, album_id):
        id = self.__getGroupIdByName(group_name)
        return self.get_photos_count_in_album(owner_id=id, album_id=album_id)

    def get_photos_count_in_album(self, owner_id, album_id):
        return self.api.photos.get(owner_id=owner_id, album_id=album_id,photo_sizes=0)['count']

    def get_group_albums(self, group_name):
        id = self.__getGroupIdByName(group_name)
        return self.api.photos.getAlbums(owner_id=id)

    def get_photos(self, owner_id, album_id, count=50, offset=0):
        return self.api.photos.get(owner_id=owner_id, album_id=album_id, photo_sizes=0, count=count, offset=offset)['items']

    def __getGroupIdByName(self, group_name):
        try:
            # Multiply on -1 because group_id must have '-' prefix
            return self.api.groups.getById(group_id=group_name)[0]['id'] * -1
        except vk_api.ApiError as error:
            if error.code == 100:
                raise exceptions.GroupNotExistException(
                    "Group with name {} not exits".format(group_name))

    def get_group_photos(self, group_name, album_id):
        id = self.__getGroupIdByName(group_name)
        return self.get_photos(owner_id=id, album_id=album_id)


    def get_original_size_photos(self, owner_id, album_id, count=50, offset=0):

        urls = list()
        # Get sizes by type (y,z,x), then get url from objects
        for photo in self.get_photos(owner_id=owner_id, album_id=album_id, count=count, offset=offset):
            urls.append({
                'name': photo['id'],
                'url': photo['sizes'][-1]['url'] # Because not all albums contains 'z' type, we assume to download last element as comfortable size
            })

        return urls

    def get_group_original_size_photos(self, group_name, album_id, count=50, offset=0):
        return self.get_original_size_photos(self.__getGroupIdByName(group_name), album_id, count=count, offset=offset)
