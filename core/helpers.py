import os
import threading
import urllib.request
import config
import interface.captions as capt

class Downloader(threading.Thread):
    """Потоковый загрузчик файлов"""

    def __init__(self, queue, progress_bar=None, saving_dir=config.SAVING_DIRECTORY, saving_counter=None):
        """Инициализация потока"""
        threading.Thread.__init__(self)
        self.queue = queue
        self.progress_bar = progress_bar
        self.saving_dir = saving_dir
        self.atomic_counter = AtomicCounter() if saving_counter is None else saving_counter

    def run(self):
        """Запуск потока"""
        # Create directory before save
        if not os.path.exists(self.saving_dir):
            os.mkdir(self.saving_dir)

        while True:
            # Получаем photo из очереди
            from_queue = self.queue.get()

            # Скачиваем файл
            self.download(from_queue)

            # Отправляем сигнал о том, что задача завершена
            self.queue.task_done()

    def download(self, photo):
        """Скачиваем файл разом и сразу же сохраняем"""
        urllib.request.urlretrieve(photo['url'], capt.Photos.file_format_to_save_png.format(self.saving_dir, photo['name']))

        if self.progress_bar is not None:
            self.progress_bar.next()

        self.atomic_counter.increment()

    def download_chunk(self, photo):
        """Скачиваем файл пачками по 1кб"""
        handle = urllib.request.urlopen(photo['url'])
        fname = photo['name']

        with open(fname, "wb") as f:
            while True:
                chunk = handle.read(1024)
                if not chunk:
                    break
                f.write(chunk)


class AtomicCounter:
    """An atomic, thread-safe incrementing counter."""

    def __init__(self, initial=0):
        """Initialize a new atomic counter to given initial value (default 0)."""
        self.value = initial
        self._lock = threading.Lock()

    def increment(self, num=1):
        """
            Atomically increment the counter by num (default 1) and return the
            new value.
        """
        with self._lock:
            self.value += num
        return self.value

    def get(self):
        return self.value
