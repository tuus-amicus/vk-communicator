import os


def setup_environments_if_can():
    import importlib
    spam_loader = importlib.find_loader('config_holder')
    found = spam_loader is not None

    # Import my configs
    if found:
        import config_holder


ACCESS_TOKEN_URL = "https://oauth.vk.com/access_token?client_id={client_id}&client_secret={client_secret}&v=5.92&grant_type=client_credentials"
SAVING_DIRECTORY = "tmp/"

setup_environments_if_can()

AUTH_TOKEN = os.getenv('AUTH_TOKEN')
PUBLIC_APP_TOKEN = os.getenv('PUBLIC_APP_TOKEN')

if AUTH_TOKEN is None or PUBLIC_APP_TOKEN is None:
    raise EnvironmentError(f"AUTH_TOKEN or PUBLIC_APP_TOKEN environment variable is not set! Explicit config specification is required. ")

