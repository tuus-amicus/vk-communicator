# -*- coding: utf-8 -*-

class Photos:
    saved_photos="{} фотографий сохраненено по адресу: {}"
    file_format_to_save_png="{}{}.png"

class General:
    saving_process_started="Начинаю процесс сохранения..."
    apply_or_decline="(Введите 'n' в случае отказа или ENTER для согласия)\n> "
    wrong_number="Введено неверное число"
    accept_action="Подтвердить скачивание?"

class Groups:
    enter_name="Введите имя группы:\n> "

class Albums:
    enter_download_id="Введите id альбома из которого хотите скачать фотографии\n> "
    found_number="Найдено {} альбомов."
    id_title="\n| ID -- Название: |\n"
    empty="Альбомов больше нет."
    show_next="Показать следующие {} альбомов?"
    founded_inside = "Найдено {} в альбоме."