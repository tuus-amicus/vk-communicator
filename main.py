# -*- coding: utf-8 -*-

from queue import Queue

from progress.bar import FillingCirclesBar

import config
import exceptions
import interface.captions as capt
from core.api import VkApi
from core.helpers import AtomicCounter
from core.helpers import Downloader
from models.Albums import Albums


def save_photos_from_album(album_id, group_name):
    photos_album_count = api.get_photos_count_in_group_album(group_name, album_id)

    print(capt.Albums.founded_inside.format(photos_album_count))
    enter = input(capt.General.accept_action + capt.General.apply_or_decline)

    if 'n' in enter:
        return

    start_download(album_id=album_id, group_name=group_name, photos_count=photos_album_count)


def start_download(album_id, group_name, photos_count):
    print("Начинаю процесс сохранения...")

    # Очередь для хранения объекта фото с ссылками
    queue = Queue()

    # Запускаем прогресс бар. Размер - количество фоток в альбоме
    with FillingCirclesBar('Прогресс:', max=photos_count) as bar:
        # Счетчик скаченных файлов. Делаем атомарным
        counter = AtomicCounter()

        """
            Создаем потоки на ожидание данных в очереди, передавая:
                * прогресс бар,
                * директорию для сохранения файлов и 
                * счетчик
        """
        # Создаем 50 потоков
        for i in range(50):
            t = Downloader(queue, bar, config.SAVING_DIRECTORY, counter)
            t.setDaemon(True)
            t.start()

        retrieved_count = 0
        while photos_count != 0:

            batch_size = min(photos_count, 100)  # 100 maximum
            for photo in api.get_group_original_size_photos(album_id=album_id, group_name=group_name,
                                                            offset=retrieved_count, count=batch_size):
                # Даем очереди нужные нам ссылки для скачивания. Помещаем по 1 ссылке на фотку в очередь
                queue.put(photo)

            photos_count -= batch_size
            retrieved_count += batch_size

        # Ждем завершения работы очереди
        queue.join()

    # Останавливаем прогресс бар
    bar.finish()

    print(capt.Photos.saved_photos.format(counter.get(), config.SAVING_DIRECTORY))


def retrieve_group_albums():
    while True:
        group_name = input(capt.Groups.enter_name)
        try:
            ret = dict()
            ret['items'] = api.get_group_albums(group_name)['items']
            ret['group_name'] = group_name
            break
        except exceptions.GroupNotExistException as ex:
            print(ex)
            raise ex

    return ret


def input_album_id():
    while True:
        number = input(capt.Albums.enter_download_id)

        if number.strip().isdigit():
            return number

        print(capt.General.wrong_number)


if __name__ == '__main__':

    api = VkApi(token=config.PUBLIC_APP_TOKEN)

    try:
        # List groups
        ret = retrieve_group_albums()

        # Cache album as object
        albums = Albums(ret['items'])

        # List group titles
        albums.show_titles_with_id()

        group_name = ret['group_name']

        # Get id of downloaded album
        number = input_album_id()

        save_photos_from_album(int(number), group_name)

    except exceptions.GroupNotExistException as ex:
        exit(1)



